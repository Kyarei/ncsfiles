Aer e Sydelfa enara nacmeryd: «Enare vyncyd anastyr enaras ceclanyd re enare pertenyd acresyd eo.»
Iss navarys enrota encaro vrenryd ro iss ercelorosnerlla cevdssa.
Issos cenvadion nacmera re ermeata necarassorys encara essyd iss «cetenata nagarago» eas os.
Tarpehes ner estemyrad ner alrayd iss gostocenvertess cynsso evencoson enefa.
Asras re eravena astyr?

Endyr syno eas iss ardas enara myrad ner necarasso myn ner iss myvearys nerana.
Ner ensor navarys encaro esternyd eas iss elsse, enaro ner refetyd treca es iss ardas eas Senar ner esvrenryd es eti.
Ner encaro nacmeryd aer cypra: «Gevess ner vyncyd ner ermead enserarys ner senad erys synamyn.» Ner encaro myrad ner ensera nas vystos ner cyrcarsa nas perna.
Myron encaro nacmeryd: «Gevess ner vyncyd ner prentyd ner metraspo ner lanta cynsso vilon eas er es issos anor ner ercentrionad nemesa re geveness denemyd ci nysos iss arda syno.»