## ncsfiles

Archives of old files related to Necarasso Cryssesa.

### Naming conventions

Each file is prefixed with date info:

* PDF files (which are not modified after creation) and files without creation time metadata (such as plain text) prefix the modification time as a cþ-number.
* Other files prefix the creation time followed by the modification time.

Some files have a letter suffix (such as `.c` or `.e`) before the extension. These show which file is older or newer if two files were named with the same file name.

#### Sources for finding dates

* Metadata inside Office or LibreOffice files – includes both creation and modification times.
* Metadata inside PDF files – also include both, but they tend to be the same.
* File system metadata – works, but pretty fragile. Luckily, we can prefix the file name with the date once we find this out.
* For documents downloaded from Google Docs, the metadata for the corresponding document there.

### Edited content

Plain-text files are converted to UTF-8 with LF newlines.
